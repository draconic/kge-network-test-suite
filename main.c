/*
    main.c

    Test suite code for the networking sub system.

    This software is licensed under the GNU Lesser General
    Public License version 3. Please refer to gpl.txt and
    lgpl.txt for more information.

    Copyright (c) 2013 Draconic Entertainment
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <KiaroNetwork.h>

// Callbacks
void on_connect(KiaroNetworkClient *client)
{
    printf("Registered client connection ...\n");
}

void on_disconnect(KiaroNetworkClient *client)
{
    printf("Registered client disconnection ...\n");
    KiaroNetworkClientDestroy(client);
}

// Main
int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Usage: %s <client/server> [remote host/listen port] [remote port]\n", argv[0]);
        return 0;
    }

    KiaroNetworkServer *server = 0;
    KiaroNetworkClient *client = 0;

    _Bool is_client = 0;
    if (KiaroNetworkInitialize() != KIARO_NETWORK_ERROR_NONE)
    {
        printf("Unable to initialize the Kiaro networking interface!\n");
        return 2;
    }

    if (strcmp(argv[1],"server") == 0)
    {
        printf("Initializing the Kiaro Networking test suite as a SERVER ...\n");
        server = KiaroNetworkServerCreate("0.0.0.0", atoi(argv[2]), 32);
        if (!server)
        {
            printf("Failed to allocate server!\n");
            return 3;
        }
        server->on_connect = on_connect;
        server->on_disconnect = on_disconnect;
    }
    else
    {
        is_client = 1;
        printf("Initializing the Kiaro Networking test suite as a CLIENT ...\n");
        client = KiaroNetworkClientCreate();
        printf("Attempting to connect to remote host %s:%s and waiting for 5 seconds...\n", argv[2], argv[3]);
        unsigned int result = KiaroNetworkClientConnect(client, argv[2], atoi(argv[3]), 5000);
        if (result != KIARO_NETWORK_ERROR_NONE)
        {
            printf("Failed to connect to remote host!\n");
            KiaroNetworkDeinitialize();
            return 1;
        }

        char *string = "DRAGONS";
        KiaroNetworkClientSend(client, strlen(string)+1, (void*)string, 0);
    }

    printf("Kiaro Networking Instance Successfully Started and is now Running ...\n");
    _Bool is_running = 1;
    while (is_running)
    {
        KiaroNetworkUpdate();

        // Server code
        if (!is_client)
        {
            KiaroNetworkPacket *packet = KiaroNetworkServerReceive();
            if (packet)
            {
                printf("Received Packet from port %u\n", packet->sender->u32_port);
                KiaroNetworkPacketDestroy(packet);
            }
        }
        else
        {

        }

    }

    KiaroNetworkDeinitialize();
    return 0;
}
